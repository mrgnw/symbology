
DJANGO_DICT = {
    '>': 'ForeignKey',
    'x': 'ManyToMany',
    '-': 'OneToOne',

    '=': 'Boolean',
    "'": 'Char',
    '#': 'Date',
    '@': 'Time',
    '{': 'DateTime',
    'e': 'Email',
    '^': 'File',
    '$': 'Float',
    '*': 'Image',
    '1': 'Integer',
    '.': 'GenericIPAddress',
    '≈': 'NullBoolean',
    '/': 'Slug',
    '"': 'Text',
    ':': 'URL',
}

symbols = list(DJANGO_DICT.keys())
FIELD_DICT = {y: x for x, y in DJANGO_DICT.items()}
fields = list(FIELD_DICT.keys())


# Load the file
# Scan line by line?
# 1 = Model
# 2 = Field
# 3 = Relation or (Relation)


'''
STATES
Nowhere
In Model
In relation
In field
'''

'''
Models
fields
relations
'''

