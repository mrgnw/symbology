# Keyboard map

	Number Keys
	` 1 2 3 4 5 6 7 8 9 0 - =	
	~ ! @ # $ % ^ & * ( ) _ +   shift

	  ¡ ™ £ ¢ ∞ § ¶ • ª º – ≠   alt
	  ⁄ € ‹ › ﬁ ﬂ ‡ ° · ‚ — ±   alt shift

	Symbols on keyboard
	[ ] \   ; '   , . /
	{ } |   : "   < > ?

	“ ‘ «   … æ   ≤ ≥ ÷
	” ’ »   Ú Æ   ¯ ˘ ¿


	Keyboard
	q w e r t y u i o p [ ] \
	a s d f g h j k l ; '
	z x c v b n m , . /

	alt
	œ ∑ ´ ® † ¥ ¨ ˆ ø π “ ‘ «
	å ß ∂ ƒ © ˙ ∆ ˚ ¬ … æ 
	Ω ≈ ç √ ∫ ˜ µ ≤ ≥ ÷

	shift
	Œ „ ´ ‰ ˇ Á ¨ ˆ Ø ∏ ” ’ »
	Å Í Î Ï ˝ Ó Ô  Ò Ú Æ
	¸ ˛ Ç ◊ ı ˜ Â ¯ ˘ ¿


(Relational algebra)[https://en.wikipedia.org/wiki/Relational_algebra]