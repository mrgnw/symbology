# Relational markup

## Django

    > ForeignKey
    x ManyToMany
    : OneToOne

    = Boolean   =±≠?b
    ' Char      c
    # Date      d []
    @ Time      t 
    { DateTime  dt #@ _ [] [@]
    e Email
    ^ File
    $ Float    . # %
    * Image
    1 Integer    # i
    . GenericIPAddress
    ≈ NullBoolean 0 Ω °
    / Slug    
    " Text    ¶
    : URL     

### Roadmap

     ! BigInt 
     ± Binary 
     % Decimal
     + PositiveInteger 
    +i PositiveSmallInteger
     i SmallInteger

### Currently unsupported

    a AutoField
    , CommaSeparatedInteger
    \ FilePath
    . IPAddress (deprecated)

## Primitive data types

    “   string
    ‘   char
    1   int •
    #   float

     0  bool    ✓ ± 0
    [ ] list    …
    (,)	tuple
    {:}	dictionary

## Payloads
    
    {:}  JSON
     -   YAML
    </>  XML